package com.natcha.hobbies.viewmodels

import androidx.lifecycle.*
import com.natcha.hobbies.database.hobby.Hobby
import com.natcha.hobbies.database.hobby.HobbyDao
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch

class HobbyViewModel(private val hobbyDao: HobbyDao): ViewModel() {
    val allHobbies: LiveData<List<Hobby>> = hobbyDao.getAll().asLiveData()
    val allHobbiesByRank: LiveData<List<Hobby>> = hobbyDao.getAllByRank().asLiveData()
    private fun insertHobby(hobby: Hobby) {
        viewModelScope.launch {
            hobbyDao.insert(hobby)
        }
    }

    private fun getNewHobbyEntry(hobbyName: String): Hobby {
        return Hobby(
            hobbyName = hobbyName,
            times = 0
        )
    }

    suspend fun getAllHobbies(): List<Hobby> {
        return hobbyDao.getAllHobbies()
    }

    fun addNewHobby(hobbyName: String){
        val newHobby = getNewHobbyEntry(hobbyName)
        insertHobby(newHobby)
    }

    fun retrieveItem(id: Int): LiveData<Hobby> {
        return hobbyDao.getHobby(id).asLiveData()
    }

    fun updateHobby(hobbyId: Int, hobbyName: String, times: Int){
        val updateItem = getUpdateItemEntry(hobbyId, hobbyName, times)
        updateItem(updateItem)
    }

    fun updateTimes(hobbyId: Int, times: Int){
        CoroutineScope(Dispatchers.IO).launch {
            hobbyDao.updateTimes(hobbyId, times)
        }
    }

    private fun getUpdateItemEntry(hobbyId: Int, hobbyName: String, times: Int): Hobby {
        return Hobby(
            id = hobbyId,
            hobbyName = hobbyName,
            times = times
        )
    }

    fun updateTimes(hobby: Hobby) {
        val newHobby = hobby.copy(times = hobby.times+1)
        updateItem(newHobby)
    }


    private fun updateItem(hobby: Hobby) {
        viewModelScope.launch {
            hobbyDao.update(hobby)
        }
    }

    fun deleteHobby(hobby: Hobby) {
        viewModelScope.launch {
            hobbyDao.delete(hobby)
        }
    }

    fun isEntryValid(hobbyName: String): Boolean {
        if(hobbyName.isBlank()) {
            return false
        }
        return true
    }

}

class HobbyViewModelFactory(
    private val hobbyDao: HobbyDao
): ViewModelProvider.Factory{
    override fun <T: ViewModel> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(HobbyViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return HobbyViewModel(hobbyDao) as T
        }
        throw IllegalAccessException("Unknown ViewModel class")
    }
}