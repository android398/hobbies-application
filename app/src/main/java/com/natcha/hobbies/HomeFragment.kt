package com.natcha.hobbies

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavDirections
import androidx.navigation.findNavController
import com.natcha.hobbies.database.hobby.Hobby
import com.natcha.hobbies.databinding.FragmentHomeBinding
import com.natcha.hobbies.viewmodels.HobbyViewModel
import com.natcha.hobbies.viewmodels.HobbyViewModelFactory
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class HomeFragment : Fragment() {
    private val viewModel: HobbyViewModel by activityViewModels{
        HobbyViewModelFactory(
            (activity?.application as HobbyApplication).database.hobbyDao()
        )
    }
    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var checkNull = false //check null hobbiesList
        lateinit var hobby1: Hobby
        var isRandom = false

        //Random
        binding?.btnRandom?.setOnClickListener {
            lifecycleScope.launch(Dispatchers.Main) {
                val hobbiesList = viewModel.getAllHobbies()
                if (hobbiesList.isEmpty()) {
                    checkNull = true
                    binding?.textviewRandomResult?.text = getString(R.string.dont_have_any_hobbies)
                } else {
                    val hobby = hobbiesList.shuffled().first()
                    hobby1 = hobby //use for update hobby in btnOk
                    binding?.textviewRandomResult?.text = hobby.hobbyName.toString()
                    isRandom = true
                }
            }
        }

        //OK
        binding?.btnOk?.setOnClickListener {
            if(!checkNull && isRandom) {
                viewModel.updateTimes(hobby1)
                binding?.textviewRandomResult?.text = ""
                isRandom = false
            }
        }

        //Bottom Navigation Menu
        binding?.bottomNavigationView?.setOnItemReselectedListener {
            lifecycleScope.launch(Dispatchers.Main){
                lateinit var action: NavDirections
                val hobbiesList = viewModel.getAllHobbies()
                if(it.itemId == R.id.ic_hobbies_list && hobbiesList.isEmpty()) {
                    action = HomeFragmentDirections.actionHomeFragmentToHobbiesListFragment()
                }else{
                    when(it.itemId) {
                        R.id.ic_hobbies_list -> action = HomeFragmentDirections.actionHomeFragmentToFullHobbiesListFragment()
                        R.id.ic_ranking -> action = HomeFragmentDirections.actionHomeFragmentToRankingFragment()
                    }
                }
                view.findNavController().navigate(action)
            }
        }
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }
}