package com.natcha.hobbies.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.natcha.hobbies.R
import com.natcha.hobbies.database.hobby.Hobby
import com.natcha.hobbies.databinding.HobbiesListItemBinding
import com.natcha.hobbies.databinding.RankItemBinding
import kotlin.coroutines.coroutineContext

class HobbiesAdapter(private val onItemClicked: (Hobby) -> Unit):
    ListAdapter<Hobby, HobbiesAdapter.HobbiesViewHolder>(DiffCallback){
    companion object {
        private val DiffCallback = object : DiffUtil.ItemCallback<Hobby>(){
            override fun areItemsTheSame(oldItem: Hobby, newItem: Hobby): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Hobby, newItem: Hobby): Boolean {
                return oldItem == newItem
            }

        }
    }

    class HobbiesViewHolder(private var binding: HobbiesListItemBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(hobby: Hobby) {
            binding.hobbyName.text = hobby.hobbyName
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HobbiesViewHolder {
        val viewHolder = HobbiesViewHolder(
            HobbiesListItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
        viewHolder.itemView.setOnClickListener{
            val position = viewHolder.adapterPosition
            onItemClicked(getItem(position))
        }
        viewHolder.itemView.findViewById<ImageView>(R.id.imageView).setOnClickListener {
            val position = viewHolder.adapterPosition
            onItemClicked(getItem(position))
        }
        return viewHolder
    }



    override fun onBindViewHolder(holder: HobbiesViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}