package com.natcha.hobbies.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.natcha.hobbies.R
import com.natcha.hobbies.database.hobby.Hobby
import com.natcha.hobbies.databinding.RankItemBinding

class HobbiesByRankAdapter(private val onItemClicked: (Hobby) -> Unit):
    ListAdapter<Hobby, HobbiesByRankAdapter.HobbiesByRankViewHolder>(DiffCallback){
    companion object {
        private val DiffCallback = object : DiffUtil.ItemCallback<Hobby>(){
            override fun areItemsTheSame(oldItem: Hobby, newItem: Hobby): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Hobby, newItem: Hobby): Boolean {
                return oldItem == newItem
            }

        }
    }

    class HobbiesByRankViewHolder(private var binding: RankItemBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(hobby: Hobby) {
            binding.hobbyName2.text = hobby.hobbyName
            binding.itemsTextview.text = hobby.times.toString()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HobbiesByRankViewHolder {
        val viewHolder = HobbiesByRankViewHolder(
            RankItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
        viewHolder.itemView.setOnClickListener{
            val position = viewHolder.adapterPosition
            onItemClicked(getItem(position))
        }
        return viewHolder
    }

    override fun onBindViewHolder(holder: HobbiesByRankViewHolder, position: Int) {
        holder.bind(getItem(position))
        if(position==0){
            holder.itemView.setBackgroundColor(Color.parseColor("#4fa5d5"))
        }
        if (position==1){
            holder.itemView.setBackgroundColor(Color.parseColor("#4CAF50"))
        }
        if (position==2){
            holder.itemView.setBackgroundColor(Color.parseColor("#8BC34A"))
        }
    }
}