package com.natcha.hobbies

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.natcha.hobbies.databinding.FragmentHobbiesListBinding
import com.natcha.hobbies.databinding.FragmentHomeBinding

class HobbiesListFragment : Fragment() {
    private var _binding: FragmentHobbiesListBinding? = null
    private val binding get() = _binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentHobbiesListBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.btnAddHobby?.setOnClickListener {
            val action = HobbiesListFragmentDirections.actionHobbiesListFragmentToAddHobbyFragment()
            view.findNavController().navigate(action)
        }
    }
    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }
}