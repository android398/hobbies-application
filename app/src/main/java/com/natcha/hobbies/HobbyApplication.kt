package com.natcha.hobbies

import android.app.Application
import com.natcha.hobbies.database.AppDatabase

class HobbyApplication: Application() {
    val database: AppDatabase by lazy {
        AppDatabase.getDatabase(this)
    }
}