package com.natcha.hobbies

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.NavOptions
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.natcha.hobbies.databinding.FragmentAddHobbyBinding
import com.natcha.hobbies.databinding.FragmentHomeBinding
import com.natcha.hobbies.viewmodels.HobbyViewModel
import com.natcha.hobbies.viewmodels.HobbyViewModelFactory
import kotlinx.coroutines.launch

class AddHobbyFragment : Fragment() {
    private val viewModel: HobbyViewModel by activityViewModels{
        HobbyViewModelFactory(
            (activity?.application as HobbyApplication).database.hobbyDao()
        )
    }
    private var _binding: FragmentAddHobbyBinding? = null
    private val binding get() = _binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentAddHobbyBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.btnDone?.setOnClickListener {
            addNewHobby()
        }
    }
    private fun isEntryValid(): Boolean {
        return viewModel.isEntryValid(
            binding?.edittextYourHobby?.text.toString()
        )
    }
    private fun addNewHobby() {
        if (isEntryValid()) {
            viewModel.addNewHobby(
                binding?.edittextYourHobby?.text.toString()
            )
            Toast.makeText(context, "Add Successfully!", Toast.LENGTH_SHORT).show()
            lifecycleScope.launch(){
                if(viewModel.getAllHobbies().size == 1){
                    val action = AddHobbyFragmentDirections.actionAddHobbyFragmentToFullHobbiesListFragment()
                    findNavController().navigate(action)
                }else {
                    findNavController().navigateUp()
                }
            }
        }
    }
    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }
}