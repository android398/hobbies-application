package com.natcha.hobbies.database.hobby

import androidx.room.*
import kotlinx.coroutines.flow.Flow

@Dao
interface HobbyDao {
    @Insert
    suspend fun insert(hobby: Hobby)

    @Update
    suspend fun update(hobby: Hobby)

    @Delete
    suspend fun delete(hobby: Hobby)

    @Query("UPDATE hobby SET times = :times WHERE id = :id")
    fun updateTimes(id: Int, times: Int)

    @Query("SELECT * FROM hobby WHERE id = :id")
    fun getHobby(id: Int): Flow<Hobby>

    @Query("SELECT * FROM hobby")
    suspend fun getAllHobbies(): List<Hobby>

    @Query("SELECT * FROM hobby")
    fun getAll(): Flow<List<Hobby>>

    @Query("SELECT * FROM hobby ORDER BY times DESC")
    fun getAllByRank(): Flow<List<Hobby>>
}