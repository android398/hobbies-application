package com.natcha.hobbies.database.hobby

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Hobby(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    @NonNull @ColumnInfo(name = "hobby_name")
    val hobbyName: String,
    @NonNull @ColumnInfo(name = "times")
    val times: Int
)
