package com.natcha.hobbies

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.natcha.hobbies.adapter.HobbiesAdapter
import com.natcha.hobbies.adapter.HobbiesByRankAdapter
import com.natcha.hobbies.databinding.FragmentHomeBinding
import com.natcha.hobbies.databinding.FragmentRankingBinding
import com.natcha.hobbies.viewmodels.HobbyViewModel
import com.natcha.hobbies.viewmodels.HobbyViewModelFactory

class RankingFragment : Fragment() {
    private val viewModel: HobbyViewModel by activityViewModels{
        HobbyViewModelFactory(
            (activity?.application as HobbyApplication).database.hobbyDao()
        )
    }
    private var _binding: FragmentRankingBinding? = null
    private val binding get() = _binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentRankingBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.recyclerView2?.layoutManager = LinearLayoutManager(requireContext())
        val hobbiesByRankAdapter = HobbiesByRankAdapter { item ->
        }
        binding?.recyclerView2?.adapter = hobbiesByRankAdapter
        viewModel.allHobbiesByRank.observe(this.viewLifecycleOwner) { items ->
            items.let {
                hobbiesByRankAdapter.submitList(it)
            }
        }
    }
    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }
}