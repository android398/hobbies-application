package com.natcha.hobbies


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.natcha.hobbies.adapter.HobbiesAdapter
import com.natcha.hobbies.database.hobby.Hobby
import com.natcha.hobbies.databinding.FragmentFullHobbiesListBinding
import com.natcha.hobbies.viewmodels.HobbyViewModel
import com.natcha.hobbies.viewmodels.HobbyViewModelFactory
import kotlinx.coroutines.launch

class FullHobbiesListFragment : Fragment() {
    private val viewModel: HobbyViewModel by activityViewModels{
        HobbyViewModelFactory(
            (activity?.application as HobbyApplication).database.hobbyDao()
        )
    }
    private var _binding: FragmentFullHobbiesListBinding? = null
    private val binding get() = _binding!!
    lateinit var hobby: Hobby

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentFullHobbiesListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())

        //One hobby
        val hobbiesAdapter = HobbiesAdapter { item ->
                hobby = item
                showConfirmationDialog()
        }
        binding.recyclerView.adapter = hobbiesAdapter
        viewModel.allHobbies.observe(this.viewLifecycleOwner) { items ->
            items.let {
                hobbiesAdapter.submitList(it)
            }
        }
        binding.btnAddHobby.setOnClickListener {
            val action = FullHobbiesListFragmentDirections.actionFullHobbiesListFragmentToAddHobbyFragment()
            view.findNavController().navigate(action)
        }
    }

    private fun deleteHobby() {
        viewModel.deleteHobby(hobby)
        lifecycleScope.launch {
            if(viewModel.getAllHobbies().isEmpty()){
                val action = FullHobbiesListFragmentDirections.actionFullHobbiesListFragmentToHobbiesListFragment()
                findNavController().navigate(action)
            }
        }
    }

    private fun showEditDialog(hobby: Hobby) {
        val inflater = LayoutInflater.from(context)
        val v = inflater.inflate(R.layout.edit_hobby, null)
        val editText = v.findViewById<EditText>(R.id.edit_hobby_name)
        MaterialAlertDialogBuilder(requireContext())
            .setView(v)
            .setCancelable(false)
            .setNegativeButton(getString(R.string.cancel)) { _, _ -> }
            .setPositiveButton(getString(R.string.ok_button_text)) { _, _ ->
                updateHobby(hobby.id, editText.text.toString(), hobby.times)
            }
            .show()
    }

    private fun showConfirmationDialog() {
        MaterialAlertDialogBuilder(requireContext())
            .setMessage(getString(R.string.delete_question))
            .setCancelable(false)
            .setNegativeButton(getString(R.string.no)) { _, _ -> }
            .setPositiveButton(getString(R.string.yes)) { _, _ ->
                deleteHobby()
            }
            .show()
    }

    private fun updateHobby(hobbyId: Int, hobbyName: String, times: Int) {
        viewModel.updateHobby(
            hobbyId,
            hobbyName,
            times
        )
    }
    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }
}